import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { Hero } from '../hero';
import { HeroService } from '../hero.service';
import { HEROES } from '../mock-heroes';

@Component({
  selector: 'app-hero-create',
  templateUrl: './hero-create.component.html',
  styleUrls: [ './hero-create.component.css' ]
})
export class HeroCreateComponent implements OnInit {
  hero: Hero = {} as Hero;

  constructor(
    private route: ActivatedRoute,
    private heroService: HeroService,
    private location: Location
  ) {}

  ngOnInit(): void {
  }

  create() {
    this.heroService.create(this.hero.name,this.hero.power);
  }

  goBack(): void {
    this.location.back();
  }
}